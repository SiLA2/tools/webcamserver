$path = Get-ChocolateyPath -PathType 'PackagePath'
$desktop = [Environment]::GetFolderPath("Desktop")
Install-ChocolateyShortcut -ShortcutFilePath "$desktop\Webcam Server.lnk" -TargetPath "$path\tools\WebcamServer.exe"