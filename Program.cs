﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2.Server;

namespace Hsrm.Sila2.WebcamService
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Bootstrapper.Start(args).RunUntilSigInt();
        }
    }
}
