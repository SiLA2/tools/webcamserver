﻿using Hsrm.Sila2.WebcamService.Contracts;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;

namespace Hsrm.Sila2.WebcamService.Implementation
{
    [Export(typeof(ICameraService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    internal class CameraService : ICameraService
    {

        [return: ContentType("image", "png")]
        public Stream TakePicture()
        {
            using (var _capture = new VideoCapture())
            {
                _capture.Open(0);
                if (_capture.IsOpened())
                {
                    var frame = new Mat();
                    var ms = new MemoryStream();
                    try
                    {
                        _capture.Read(frame);
                        frame.WriteToStream(ms);
                        ms.Position = 0;
                        return ms;
                    }
                    finally
                    {
                        frame.Dispose();
                    }
                }
            }
            throw new Exception("Camera image unavailable");
        }
    }
}
