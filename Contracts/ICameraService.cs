﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;

namespace Hsrm.Sila2.WebcamService.Contracts
{
    /// <summary>
    /// Denotes a service to obtain CPU utilization details
    /// </summary>
    [SilaFeature]
    public interface ICameraService
    {
        /// <summary>
        /// Takes a picture with the default video device
        /// </summary>
        /// <returns>A binary with the image</returns>
        [return: ContentType("image", "png")] Stream TakePicture();
    }
}
